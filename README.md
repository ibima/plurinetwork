# README #

This project archives and provides a computationally derived network
describing the presumed molecular interaction of genes to maintain
the pluripotency of stem cells. This work was published in 2010 as

Anup Som , Clemens Harder , Boris Greber, Marcin Siatkowski, Yogesh Paudel, Gregor Warsow, Clemens Cap, Hans Sch�ler, Georg Fuellen 
(2010 "The PluriNetWork: An Electronic Representation of the Network Underlying Pluripotency in Mouse, and Its Applications" PLoS ONE,
http://dx.doi.org/10.1371/journal.pone.0015165

### How do I get set up? ###

This package is not yet available as an R package. Please download the
source tree of this project (https://bitbucket.org/ibima/plurinetwork/get/master.tar.gz) and use "R INSTALL <zip/tarball>".
It provides an igraph object of that network.

### Who do I talk to? ###

Please contact Prof. Georg Fuellen at <ibima-sekretariat{bei}uni-rostock.de>